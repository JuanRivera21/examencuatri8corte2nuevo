import  express  from 'express';
import  json  from 'body-parser';
import { render } from 'ejs';


export const router = express.Router();

router.get('/',(req,res)=>{

    res.render('index',{titulo:"Examen Corte 2"})

})

router.post('/recibo',(req,res)=>{

    const params = {
        numDocente:req.body.numDocente,
        nombre:req.body.nombre,
        domicilio:req.body.domicilio,
        nivel:req.body.nivel,
        pagohr:req.body.pagohr,
        horas:req.body.horas,
        hijos:req.body.hijos
    }

    res.render('recibo',params);
})

router.get('/recibo',(req,res)=>{

    const params = {
        numDocente:req.query.numDocente,
        nombre:req.query.nombre,
        domicilio:req.query.domicilio,
        nivel:req.query.nivel,
        pagohr:req.query.pagohr,
        horas:req.query.horas,
        hijos:req.query.hijos
    }

    res.render('recibo',params);
})

export default {router};